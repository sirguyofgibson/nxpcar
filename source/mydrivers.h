/*
 * mydrivers.h
 *
 *  Created on: Feb 14, 2018
 *      Author: root
 */

#ifndef MYDRIVERS_H_
#define MYDRIVERS_H_

//#define _debug_
//#define _lab_5_part_1_
//#define _lab_5_part_2_
//#define ADC_TEMP
//#define _lab_5_part_3_
#define _servo_drive_
#define _motor_control_
#define camera
//#define _stepper_control_
//#define filtering
//#define ADC

#define BAUD_RATE 9600      //default baud rate
//#define SYSTEM_CLOCK 20485760 //default system clock

void initialize(void);

void btns_init(void);
void leds_init(void);

void PORTA_IRQHandler(void);
void PORTC_IRQHandler(void);

void adc_init(void);
void ADC1_IRQHandler(void);

void ftm_init(void);
void FTM0_IRQHandler(void);

void pdb_init(void);
void PDB0_IRQHandler(void);

void pit_init(void);
void PIT0_IRQHandler(void);

void rtc_init(void);
void RTC_Seconds_IRQHandler(void);

void uart_init(void);//polling, no interrupt yet
char uart_getchar(void);
void uart_putchar(char ch);
void uart_putstr(char *ptr_str);

#define WAIT(_x) for(int _a=0; _a<_x; _a++){}

#define N 7
#define N_LOG 1
extern volatile unsigned char PIXEL_VALUES[128];
extern volatile signed char OUT_BUFFER[128];

#define CENTER_SIZE 8
extern volatile unsigned char centers[CENTER_SIZE];
unsigned char filter_center(unsigned char);

#define LOW_CAMERA 10
#define HIGH_CAMERA 100

#define SERVO_LOW 2800
#define SERVO_NEUTRAL 3800
#define SERVO_HIGH 4650
#define SERVO_STEP 20
extern volatile unsigned int SERVO_POS;
#define MOVE_SERVO(VALUE) do{SERVO_POS = VALUE; FTM3->CONTROLS[0].CnV = VALUE;} while(0)

extern volatile unsigned char MOTOR_LEFT;
extern volatile unsigned char MOTOR_RIGHT;
#define RIGHT_MOTOR(_speed) do{MOTOR_RIGHT = _speed; FTM0->CONTROLS[6].CnV = MOTOR_RIGHT;}while(0)
#define LEFT_MOTOR(_speed) do{MOTOR_LEFT = _speed; FTM0->CONTROLS[7].CnV = MOTOR_LEFT;}while(0)
#define SET_MOTORS(_speed) do{MOTOR_RIGHT = _speed;MOTOR_LEFT = _speed;\
		FTM0->CONTROLS[6].CnV = MOTOR_RIGHT;FTM0->CONTROLS[7].CnV = MOTOR_LEFT;}while(0)

extern volatile unsigned char MODE;

#endif /* MYDRIVERS_H_ */
