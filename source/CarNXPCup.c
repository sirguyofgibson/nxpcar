/*
 * NXP Cup Challenge
 * Nicholas Amatruda
 *
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "MK64F12.h"
#include "mydrivers.h"

#define K 30
#define D 50

#define S 150
#define Q 5

int main(void) {

	initialize();


//	int clock = SystemCoreClock;
//	int clkdiv = SIM->CLKDIV1;
//
	char string [515];
//	char input;
//	sprintf(string, "Core Clock Speed: %d\r\nBUS Clock Divide: 0x%x\r\n", clock, clkdiv);
//	uart_putstr(string);
	signed char true_north = 64;
//	uart_putstr(string);
	signed char output[128];


	while(1){
	if(MODE==1){

		for(int i=N; i<128-N; i++){
			unsigned int x = PIXEL_VALUES[i];
			for(int x=0;x<N;x++){
				x += PIXEL_VALUES[i-x-1];
			}
			OUT_BUFFER[i] = x>>1;

//			diff = OUT_BUFFER[i+1] - OUT_BUFFER[i];
//			output[i] = diff;
		}

		int left = OUT_BUFFER[N];
		int right = OUT_BUFFER[127-N];
		char mini, maxi = 0;
		signed char min = 255;
		signed char max = 0;
		signed char diff = 0;
		for(int i=0; i<128; i++){
			if((i < N) | (i > 127-N)){
				//flatten edges of data
				OUT_BUFFER[i] = left;
				OUT_BUFFER[127-i] = right;
				output[i] = 0;
				output[127-i] = 0;
			}else{
				diff = OUT_BUFFER[i+1]-OUT_BUFFER[i-1];
				output[i] = diff;
				if(diff > max){
					max = diff;
					maxi = i;
				}else if(diff < min){
					min = diff;
					mini = i;
				}
			}
		}
		char raw = (mini + maxi)>>1;
		char center = filter_center(raw);

//		signed char x = filter_center();
//		if((maxi<LOW_CAMERA)&(mini>HIGH_CAMERA)){
//			MOVE_SERVO(SERVO_NEUTRAL);
//		}else{
		MOVE_SERVO(SERVO_NEUTRAL-K*(center-true_north)-D*(raw-center));
//		}
		RIGHT_MOTOR(S+Q*(center-true_north));
		LEFT_MOTOR(S-Q*(center-true_north));
//		for(int i=0; i<128; i++){
//			sprintf(string, "%d ", output[i]);
//			uart_putstr(string);
//		}
//		sprintf(string, "\r\n");
//		uart_putstr(string);
	}else{
		SET_MOTORS(0);
		MOVE_SERVO(SERVO_NEUTRAL);
	}
	}//end main loop
	return 0;
}




//From NXP Template   ;For Reference
//#include "board.h"
//#include "peripherals.h"
//#include "pin_mux.h"
//#include "clock_config.h"
//
//{
//  	/* Init board hardware. */
//    BOARD_InitBootPins();
//    BOARD_InitBootClocks();
//    BOARD_InitBootPeripherals();
//  	/* Init FSL debug console. */
//    BOARD_InitDebugConsole();
//}
