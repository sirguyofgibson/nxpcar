/*
 * mydrivers.c
 *	-TPM interrupt
 *
 *  Created on: Feb 13, 2018
 *  Author: Nicholas Amatruda
 *  Class: IDE @ RIT CMPE-460
 */
#include "MK64F12.h"
#include "mydrivers.h"
#include <stdio.h>


volatile unsigned char PIT_CNT = 0;
volatile unsigned char RISING = 1;
volatile unsigned char SI_RESET = 1;
volatile unsigned int ADC_VALUE = 0;
volatile unsigned char PIXEL_VALUES[128];
volatile signed char OUT_BUFFER[128];
#define READ_PIXEL do{PIXEL_VALUES[PIT_CNT] = ADC1->R[0];} while(0)
#define TRIGGER_ADC do{ADC1->SC1[0] = ADC_SC1_ADCH(3) | ADC_SC1_AIEN_MASK;} while(0)
volatile unsigned char notfiltering = 1;
volatile unsigned int SERVO_POS = SERVO_NEUTRAL;

volatile unsigned char MOTOR_LEFT = 0;
volatile unsigned char MOTOR_RIGHT = 0;
volatile unsigned char MODE = 0;


volatile unsigned char centers[CENTER_SIZE];
unsigned char old_center = 0;
unsigned char filter_center(unsigned char newc){
	if(old_center == CENTER_SIZE){
		old_center = 0;
	}
	centers[old_center] = newc;
	unsigned int sum = 0;
	for(int i=0; i<CENTER_SIZE; i++){
		sum += centers[i];
	}
	old_center++;
	return (char)(sum>>3);
}



char center(void){
	char start, end = 0;
	for(int i=0; i<128; i++){
		if(PIXEL_VALUES[i]=255){
			if(start == 0){
				start = i;
			}else{
				end = i;
			}

		}

	}
	return (end - start);
}

void initialize(void){
	__asm volatile ("cpsid i");

	uart_init();
	ftm_init();
	pit_init();
	adc_init();

	FTM3->SC |= FTM_SC_CLKS(1);//start flex timer
	PIT->CHANNEL[0].TCTRL |= PIT_TCTRL_TEN_MASK;//start PIT

	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	PORTB->PCR[18] = PORT_PCR_MUX(1);
	GPIOB->PDDR |= 1<<18;

	//Button interrupt
	PORTA->PCR[4] = PORT_PCR_MUX(1) | PORT_PCR_IRQC(10);//rising edge int

	NVIC_EnableIRQ(PORTA_IRQn);
	NVIC_EnableIRQ(FTM3_IRQn);
	NVIC_EnableIRQ(PIT0_IRQn);
	NVIC_EnableIRQ(ADC1_IRQn);
	__asm volatile ("cpsie i");
}

void adc_init(void){
	SIM->SCGC3 |= SIM_SCGC3_ADC1_MASK;
	SIM->SOPT7 |= SIM_SOPT7_ADC1ALTTRGEN_MASK | SIM_SOPT7_ADC1TRGSEL(15);
	//ADC1->CFG1 |= ADC_CFG1_MODE(3);
	unsigned int calib;//calibration routine
	ADC1->SC3 = ADC_SC3_CAL_MASK;
	while ( (ADC1->SC3 & ADC_SC3_CAL_MASK) != 0 ){}
		calib = ADC1->CLP0;
		calib += ADC1->CLP1;
		calib += ADC1->CLP2;
		calib += ADC1->CLP3;
		calib += ADC1->CLP4;
		calib += ADC1->CLPS;
		calib = calib >> 1;
		calib |= 0x8000;
		ADC1->PG = calib;
	//armed for software triggering
}

void ADC1_IRQHandler(void){
	READ_PIXEL;
}

void PORTA_IRQHandler(void){//Button Press
	int sw3 = PORTA->ISFR & PORT_ISFR_ISF(1<<4);

	if(sw3){
		PORTA->ISFR |= 1<<4;
		if(MODE == 0){
			MODE = 1;
		}else{
			MODE = 0;//toggle between go and stop
		}
	}
}

void rtc_init(void){
	//enable real time clock output to Port E Pin 26
	SIM->SOPT2 &= 0;//~SIM_SOPT2_RTCCLKOUTSEL_MASK;//1hz
	SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
	SIM->SCGC6 |= SIM_SCGC6_RTC_MASK;

	PORTE->PCR[26] = PORT_PCR_MUX(6);//|PORT_PCR_IRQC(9);//Rising-edge interrupt port E

	RTC->CR |= RTC_CR_SWR_MASK;
	RTC->CR = 0;

	RTC->CR |= RTC_CR_OSCE_MASK;//enable 32,768 Hz oscillator
	RTC->TPR = 1;
	RTC->TSR = 1;//should clear tif...!
#ifdef _debug_
	RTC->SR |= RTC_SR_TCE_MASK;//enable timer
#elif defined(_lab_5_part_1_)
	RTC->SR &= ~RTC_SR_TCE_MASK;//disable timer
#endif
	RTC->IER |= RTC_IER_TSIE_MASK;
}

void RTC_Seconds_IRQHandler(void){
#ifdef _debug_
	uart_putstr("second from rtc\r\n");
#endif
}

void ftm_init(void){
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK|SIM_SCGC5_PORTD_MASK;
	SIM->SCGC6 |= SIM_SCGC6_FTM0_MASK;
	SIM->SCGC3 |= SIM_SCGC3_FTM3_MASK;

		//pin 1		int	rising edge		FTM0
	PORTA->PCR[1] = (PORT_PCR_MUX(3)); //| PORT_PCR_IRQC(9));// | PORT_PCR_DSE_MASK );
	PORTA->PCR[2] = (PORT_PCR_MUX(3));

	FTM0->CNTIN = 0;
	FTM0->MOD |= 242;

	//Pulse out on Channel 6; Port A Pin 1
	//Channel 6					edge-align			high-true pulse on output CnV match
	FTM0->CONTROLS[6].CnSC |= ( FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB(1) | FTM_CnSC_ELSA(0));//FTM_CnSC_CHIE_MASK
	FTM0->CONTROLS[6].CnV |= 0;
	//pulse-width = CnV - CNTIN = CnV
	//PORT A Pin 2
	FTM0->CONTROLS[7].CnSC |= ( FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB(1) | FTM_CnSC_ELSA(0));//FTM_CnSC_CHIE_MASK
	FTM0->CONTROLS[7].CnV |= 0;

	PORTD->PCR[0] = PORT_PCR_MUX(4);//Servo
	PORTD->PCR[1] = PORT_PCR_MUX(4);//SI pulse

	FTM3->MOD = 52429;
	FTM3->CONTROLS[0].CnSC |= ( FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB(1) | FTM_CnSC_ELSA(0));
	FTM3->CONTROLS[0].CnV = SERVO_NEUTRAL;

	//SI pulse
	FTM3->CONTROLS[1].CnSC |= ( FTM_CnSC_MSB_MASK | FTM_CnSC_ELSB(1) | FTM_CnSC_ELSA(0));
	FTM3->CONTROLS[1].CnV = 75;

	FTM3->SC |=  FTM_SC_TOIE_MASK | FTM_SC_PS(3);//clock not enabled yet

				//system clock    prescale=2   interrupt enable   center align pwm
	FTM0->SC |=  FTM_SC_CLKS(1) | FTM_SC_PS(3); // );//| FTM_SC_CPWMS(1));| FTM_SC_TOIE_MASK

}

void FTM3_IRQHandler(void){
	__asm volatile ("cpsid i");

	int chi = FTM3->STATUS & FTM_STATUS_CH6F_MASK;//channel interrupt
	int tof = FTM3->SC & FTM_SC_TOF_MASK;//timer overflow interrupt

	if(tof){
		FTM3->SC &= ~FTM_SC_TOF_MASK;//clear interrupt
#ifdef camera
		PIT_CNT = 0;//reset pixel clock
		//update servo!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#endif
	}
	else if(chi){
		FTM3->CONTROLS[1].CnSC &= ~FTM_CnSC_CHF_MASK;//clear channel flag
		uart_putstr("flex channel trigger\r\n");
	}else{
		uart_putstr("flex timer interrupt\r\n");
	}

	__asm volatile ("cpsie i");
	return;
}

void pdb_init(void){
	SIM->SCGC6 |= SIM_SCGC6_PDB_MASK;

	PDB0->MOD = 300;//14.3 us conversion time?
	//PDB0->SC_CONT_MASK | PDB_SC
	//PDB0->CH[1].C1 |= PDB_C1_EN(1) | PDB_C1_TOS(1);

}

void PDB0_IRQHandler(void){
	PDB0->SC &= ~PDB_SC_PDBIF_MASK;
}

void pit_init(void){
#ifdef camera
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	//Using GPIO B 23 NOW!
	PORTB->PCR[23] = PORT_PCR_MUX(1);//GPIO
	GPIOB->PDDR |= 1<<23;
	GPIOB->PCOR = 1<<23;

	PIT->MCR = 0;//~PIT_MCR_MDIS_MASK;//enable clock
	PIT->CHANNEL[0].LDVAL = 300;//half pixel cycle time
	PIT->CHANNEL[0].TCTRL = PIT_TCTRL_TIE_MASK;//interrupt toggles gpioA2
	//not enabled to start counting yet

#endif
#ifdef __ADC
	SIM->SCGC6 |= SIM_SCGC6_PIT_MASK;
	PIT->MCR = 0;

#endif

}
void PIT0_IRQHandler(void){
	PIT->CHANNEL[0].TFLG = 1;
#ifdef camera
	if((PIT_CNT == 0)&(RISING==1)){
		RISING = 0;
		GPIOB->PSOR = 1<<23;
	}else if((PIT_CNT == 0)&(RISING==0)&(SI_RESET==1)){
		PIT_CNT = 1;
		SI_RESET = 0;
	}else if((PIT_CNT == 1)&(SI_RESET==0)){
		RISING = 1;
		GPIOB->PCOR = 1<<23;
		SI_RESET=1;
		//begin ADC conversion
		TRIGGER_ADC;
	}else if(PIT_CNT != 129){
		if(RISING){
			GPIOB->PSOR = 1<<23;//turn pin on
			RISING = 0;
			//begin ADC conversion
			TRIGGER_ADC;
		}else{
			GPIOB->PCOR = 1<<23;//turn pin off
			PIT_CNT ++;
			RISING = 1;
		}
	}else{
		GPIOB->PCOR = 1<<23;//clear
	}
#endif
}

void btns_init(void){
					//pin 4, SW3			pin 6, SW2
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK | SIM_SCGC5_PORTC_MASK;

	PORTC->PCR[6] = PORT_PCR_MUX(1) | PORT_PCR_IRQC(11);//dual edge int
	PORTA->PCR[4] = PORT_PCR_MUX(1) | PORT_PCR_IRQC(10);//rising edge int
}

void leds_init(void){
	//Blue LED Port B Pin 21
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	PORTB->PCR[21] = PORT_PCR_MUX(1);
	GPIOB->PDDR |= (1<<21);
	GPIOB->PSOR = (1<<21);
}


void PORTC_IRQHandler(void){//Button Press
	int isfr = PORTC->ISFR;
	int sw2 = isfr & PORT_ISFR_ISF(1<<6);
	if(sw2){
		PORTC->ISFR |= PORT_ISFR_ISF(1<<6);
#ifdef _debug_
		uart_putstr("Switch 2\n\r");
#elif defined(_lab_5_part_1_)
		//
		if(INITIAL==0){
			if(TIMER_ON){
				STOP_TIMER;
			}else{
				START_TIMER;
			}
			GPIOB->PTOR = (1<<21);//Toggle Blue LED
		}else{
			INITIAL --;
		}
#endif

	}else{
		char string[50];
		sprintf(string, "PORTC interrupt: %x\n\r", isfr);
		uart_putstr(string);
	}
}

void PORTE_IRQHandler(void){
	int pte = PORTE->ISFR;
	PORTE->ISFR |= PORT_ISFR_ISF(1<<26);
	char string[50];
	sprintf(string, "PORTE: %d\r\n", pte);
	uart_putstr(string);
}

void uart_init(void){
	//define variables for baud rate and baud rate fine adjust
	uint16_t ubd, brfa;

	//Enable clock for UART
	SIM->SCGC4 |= SIM_SCGC4_UART0_MASK;
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;

	//Configure the port control register to alternative 3 (which is UART mode for K64)
	PORTB->PCR[16] |= (3<<8);//receive pin
	PORTB->PCR[17] |= (3<<8);//transmit pin

	/*Configure the UART for establishing serial communication*/
	//Disable transmitter and receiver until proper settings are chosen for the UART module
	UART0->C2 = 0;

	//Select default transmission/reception settings for serial communication of UART by clearing the control register 1
	UART0->C1 = 0;

	//UART Baud rate is calculated by: baud rate = UART module clock / (16 × (SBR[12:0] + BRFD))
	//13 bits of SBR are shared by the 8 bits of UART3_BDL and the lower 5 bits of UART3_BDH
	//BRFD is dependent on BRFA, refer Table 52-234 in K64 reference manual
	//BRFA is defined by the lower 4 bits of control register, UART0_C4

	//calculate baud rate settings: ubd = UART module clock/16* baud rate
	ubd = (uint16_t)((DEFAULT_SYSTEM_CLOCK)/(BAUD_RATE * 16));

	//distribute this ubd in BDH and BDL
	UART0->BDH |= (ubd >> 8)&(0x1F);
	UART0->BDL = ubd;

	//BRFD = (1/32)*BRFA
	//make the baud rate closer to the desired value by using BRFA
	brfa = (((DEFAULT_SYSTEM_CLOCK*32)/(BAUD_RATE * 16)) - (ubd * 32));

	//write the value of brfa in UART0_C4
	UART0->C4 |= brfa & 0x1F;//5 bits

	//Enable transmitter and receiver of UART
	 UART0->C2 = 12;
}

char uart_getchar(void){
	/* Wait until there is space for more data in the receiver buffer*/
	while((UART0->S1 & UART_S1_RDRF_MASK) == 0){}
	/* Return the 8-bit data from the receiver */
	return(UART0->D);
}

void uart_putchar(char ch)
{
/* Wait until transmission of previous bit is complete */
	while((UART0->S1 & UART_S1_TDRE_MASK) == 0){}
	UART0->D = ch;
}

void uart_putstr(char *ptr_str){
	//loop through all characters until character is null (0)
	while(*ptr_str){
		uart_putchar(*ptr_str);
		ptr_str++;
	}
}

